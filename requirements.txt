django==1.4.3
django-cms==2.3.4
git+https://github.com/ondrejsika/django-form-designer.git
django-inline-ordering
easy_thumbnails
psycopg2
